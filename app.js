import express from 'express'
import http from 'http'
import axios from 'axios'
import SocketIO from 'socket.io'
import youtubeInfo from 'youtube-info'

let currentVideo = {}
let videoActive = true
const nextUrl = 'http://irc.blalabs.net/music/next.php'

let app = express()
let server = http.Server(app)
let io = new SocketIO(server)

const getInfo = (url) => {
    return new Promise((yell, cry) => {
        const ytMatch = /(youtube.com\/watch\S*v=|youtu.be\/)([\w-]+)/.exec(url)
        if (ytMatch && ytMatch[2]) {
            youtubeInfo(ytMatch[2]).then(res => {
                return yell({
                    type: "youtube",
                    id: res.videoId,
                    title: res.title,
                    duration: res.duration * 1000
                })
            })
            .catch(err => {
                return cry(err)
            })
        }
    })
}
const getNext = () => {
    return new Promise ((yell, cry) => {
        axios.get(nextUrl).then(res => {
            yell(res.data)
        })
        .catch(err => {
            cry(err)
        })
    })
}
const playNext = () => {
    return new Promise((yell, cry) => {
        getNext().then(res => {
            getInfo(res).then(res => {
                currentVideo = res
                currentVideo.startTime = new Date().getTime()
                io.emit('currentVideo', currentVideo)
                yell()
            })
        })
        .catch(err => {
            cry(err)
        })
    })
}

const videoLooper = () => {
    if (!videoActive) {
        if ("startTime" in currentVideo) {
            currentVideo.startTime = currentVideo.startTime + 1000
        }
        return setTimeout(videoLooper, 1000)
    }
    if (!("startTime" in currentVideo) || (currentVideo.startTime + currentVideo.duration) < new Date().getTime()) {
        playNext().then(() => {
            setTimeout(videoLooper, 1000)
        })
        .catch(err => {
            console.log("STUK")
        })
    } else {
        setTimeout(videoLooper, 1000)
    }
}
videoLooper()

app.get('/', (req, res) => {
    res.sendFile(__dirname+'/player.html')
})

app.get('/video/play', (req, res) => {
    videoActive = true
    io.emit('play')
    res.send("playing")
})
app.get('/video/pause', (req, res) => {
    videoActive = false
    io.emit('pause')
    res.send("paused")
})
app.get('/video/next', (req, res) => {
    playNext().then(() => {
        res.send("ok")
    }).catch(() => {
        res.send("fail")
    })
})

io.on('connection', socket => {
    io.emit('currentVideo', currentVideo)
})

server.listen(1338)
